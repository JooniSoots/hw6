
import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
	   
     // Test 1. Generating graph from realistics number of vertices, arcs. Program calculates longest path from given vertex.
	
	  System.out.println("Test nr.1. Tippude arv on 7 ja kaarte arv on 9.");
      System.out.println("Etteantud tipp on 3.");
      System.out.println();
	  Graph g1 = new Graph ("G");
      g1.createRandomSimpleGraph (7, 9);
      System.out.println (g1);
     int tipp1=3;
     g1.longestDistance(tipp1);
     System.out.println();
     
     // TODO!!! Your experiments here
     // Test 2. Generating graph from 0 verticles, 0 arcs and given vertex number is 0.

	  System.out.println("Test nr.2. Tippude arv on 0  ja kaarte arv on 0.");
      System.out.println("Etteantud tipp on 0.");
      System.out.println();
	  Graph g2 = new Graph ("G");
      g2.createRandomSimpleGraph (0, 0);
      System.out.println (g2);
     int tipp2=0;
     g2.longestDistance(tipp2);    
     System.out.println();
     
     // Test 3. Generating graph from 0 verticles and 0 arcs, given vertex number is 2.
	  
     System.out.println("Test nr.3. Tippude arv on 0 ja kaarte arv on 0.");
      System.out.println("Etteantud tipp on 2.");
      System.out.println();
	  Graph g3 = new Graph ("G");
      g3.createRandomSimpleGraph (0, 0);
      System.out.println (g3);
     int tipp3=2;
     g3.longestDistance(tipp3);
     System.out.println();
     
     // Test 4. Testing with overdimensional number of verticles.
     /*
	  System.out.println("Test nr.4. Tippude arv on 2501 ja kaarte arv on 2500.");
      System.out.println("Etteantud tipp on 3.");
      System.out.println();
	  Graph g4 = new Graph ("G");
      g4.createRandomSimpleGraph (2501, 2500);
      System.out.println (g4);
     int tipp4=3;
     g4.longestDistance(tipp4);
     System.out.println();
     */
     // Test 5 Testing with impossible number of edges.
	  System.out.println("Test nr.5. Tippude arv on 9 ja kaarte arv on 7.");
      System.out.println("Etteantud tipp on 4.");
      System.out.println();
	  Graph g5 = new Graph ("G");
      g5.createRandomSimpleGraph (9, 7);
      System.out.println (g5);
     int tipp5=4;
     g5.longestDistance(tipp5);
     System.out.println();
     
     
     
   }


   /** 
    * Task: Create method to find from simple graph longest path from given vertex.
    * Task in estonian: Koostada meetod, mis leiab etteantud sidusas lihtgraafis etteantud tipust v k�ige kaugemal asuva tipu.
    * Solution: from adjascent matrix is generated distance matrix.
    * Floyd-Warshall algorithm is used to find longest path from given vertex.
    * The length of the arc is considered as one unit.
    */
   class Vertex {

      private  String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         System.out.println("K�lgnevusmaatriks");
         System.out.println();
         printMatrix(connected);
      }
   
      
      /**
       * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * lecture materials
       * Converts adjacency matrix to the matrix of distances.
       * @param mat adjacency matrix
       * @return matrix of distances, where each edge has length 1
       */
      
      /**
       * 
       * @param a
       *http://stackoverflow.com/questions/14293263/java-arraylist-clear-function
       *http://stackoverflow.com/questions/8523901/how-to-sort-an-arraylist-in-ascending-order-using-using-collections-and-comparat
       *
       */
      public void longestDistance (int a) {
    	  if (a==0) {
    		  System.out.println("Tipu number on "+a+". Graafil "+this.id+" pole tippe, ei saa kaugust arvutada.");
    		  return;
    	  } //if number of vertex is 0.
    	  
    	  if (this.getVList() == null ||this.getVList().size() ==0){
    		  System.out.println("Tipu number on "+a+". Graafil "+this.id+" pole tippe, ei saa kaugust arvutada.");
    		  return;
    	  }  //if there is no vertices.

    	  
    	  //take from vertex list vertex with requested number
    	  Vertex alg = this.getVList().get(a-1);
    	  
			if ((!getVList().contains(alg))) //if requested vertex does not excist in the graph.
				throw new RuntimeException("the argument vertices are not represented in the graph!");
			
	    	int[][] adj = createAdjMatrix();
	    	//printMatrix(adj); //for testing
	    	
	    	int[][] dist = distMatrix(adj);
	    	System.out.println("Kauguste maatriks");
	    	System.out.println(); //additional line
	    	printMatrix(dist);  //for testing
	    	System.out.println();  //additional line
	    	
	    	//find farthest vertices and save their info.
	    	int i=a-1; //given vertex
	    	int pikim=0; //farthest distance is 0
	    	ArrayList<Integer> kaugeimadTipud = new ArrayList<Integer>();
	    	for(int j=0; j<dist.length;j++){
	    		//System.out.println(j+" j ");
	    		if(dist[i][j]>pikim){    //if element from row i is bigger than longest
	    			pikim=dist[i][j];     //is the longest element
	    			//System.out.println(pikim+" ");
	    			//clear the previous list, 
	    			kaugeimadTipud.clear();
	    			//destination vertex number became first element
	    			kaugeimadTipud.add(j);
	    			//System.out.println(j+1+" suurem");
	    		}
	    		else if(dist[i][j]!=0 && dist[i][j]== pikim){ //if there is another distance with same length, vertex number will added to list. 
	    			kaugeimadTipud.add(j);
	    			//System.out.println(j+1+" v�rdne");
	    		}
	    	} //now arrayList is created from farthest vertex/vertices
	    	
	    Collections.sort(kaugeimadTipud);
	    for(int l=0; l<kaugeimadTipud.size(); l++)
	    	System.out.println("Kaugeim tipp etteantud tipust " + a + " on "+(1+kaugeimadTipud.get(l))+" ja ");
	    	
         System.out.println("Kaugeim tipp etteantud tipust " + a + " on kaugusel "+dist[i][kaugeimadTipud.get(0)]+" �hikut.");
   }
            
  		
      
      /**
       * arraylist from vertices
       * @return
       */
	  private ArrayList<Vertex> getVList() {
  	    ArrayList<Vertex> vList = new ArrayList<Vertex>();
  		Vertex v1 = this.first;
  		
  		if(v1==null) return vList; //if no vertices
  		
  		vList.add(v1);
  		
  		while (v1.next != null) {
  			vList.add(v1.next);
  			v1 = v1.next;
		}
		return vList;
	  }
	  
	  /**
	   * Creating distance matrix
	   * @param mat
	   * @return
	   */
	  public int[][] distMatrix (int[][] mat) {
		  //System.out.println("adj:");
		  //printMatrix(mat);
		  int len=mat.length; //adjacency matrix length
		  int[][] distanceM = new int[len][len]; //create distance matrix with same size
		  
		  /**all fields, which are not on diagonal and adj.matrix value is 0, value will be eternity 
		  */
    	  for (int i=0; i < len; i++) {
              for (int j=0; j < len; j++) {
            	  //System.out.println(mat[i][j]);
        		  if (mat[i][j] == 0 && i!=j) {    			  
        			  distanceM[i][j] = Integer.MAX_VALUE;        			 
        		  }
        		  if(mat[i][j]==1){
        			  distanceM[i][j] = 1;
        		  }
              }
           }
    	  //System.out.println("dist 1:");
    	  //printMatrix(distanceM);
 
          
    	  /**
    	   * Calculating distance between vertices
    	   */
    	  for (int k=0; k < len; k++) {              
              for (int i=0; i < len; i++) {
                  for (int j=0; j < len; j++) {
                      // If i->j is not connected
                	  if (distanceM[i][k] == Integer.MAX_VALUE || distanceM[k][j] == Integer.MAX_VALUE) {
                          continue;                  
                      }
                      // if path i->j is longer than i->k->j, replace values
                      if (distanceM[i][j] > distanceM[i][k] + distanceM[k][j]) {
                    	  distanceM[i][j] = distanceM[i][k] + distanceM[k][j];
                         // System.out.println(distanceM[i][j]);
                      }
                  }
              }
    	  }
    	  //printMatrix(distanceM);
    	  return distanceM;
          }      
	  
	  /**
	   * Matrix printout to console
	   * @param a
	   */
	  public void printMatrix(int[][] a){
    	  int m=a.length;
    	  
    	  //matrix printout with vertices indexes
    	  for(int i=0; i<m; i++){
    		  System.out.print(i+" | ");
    		  for(int j=0; j<m; j++){
    			  System.out.print(a[i][j]+" ");  
    		  }
    		  System.out.println();
    	  }
    	  }  
}
}